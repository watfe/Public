#!/usr/bin/python3
# -*- coding: UTF-8 -*-


'''
Created on 忘了，大概是两年前吧，现在是2018-3-30
@author: watfe
@title: 网页访问

从开始学python用requests，学到和遇到的东西，陆陆续续，常用的基本都总结在了这里了。
字符编码、代理、网页头模拟、Cookies、Session
取文本中间、正则、BeautifulSoup
多进程下载、带进度条断点续传下载。
'''


import random,os,sys,re,multiprocessing,urllib,time,json
from html.parser import HTMLParser
import requests                     # pip install requests
from bs4 import BeautifulSoup       # pip install beautifulsoup4
from pyquery import PyQuery as pq
from tqdm import tqdm               # pip install tqdm
import codecs                       # pip install codecs
import chardet                      # pip install chardet
# import socks                        # pip install PySocks   # socks5代理用到

import ssl
ssl._create_default_https_context = ssl._create_unverified_context   # 避免SSL certificate verify 问题
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)   # 移除控制台输出InsecureRequestWarning的问题
# import socket
# socket.setdefaulttimeout(200)   # 设置200秒超时。requests加了timeout还是偶尔会卡死。网上说是出在dns解析上，根据网上方法，这里加了一个socket超时设置，实测有效，如有需要可以开启。

'''将当前工作目录 从 解释器所在目录 切换到 脚本所在目录(sublime不需要这个,vsc，pycharm需要)'''
cwd = sys.path[0]  					 # 被初始执行脚本所在目录
if os.path.isfile(cwd):  			 # py2exe打包后，路径变成 cwd+\library.zip
    cwd, filen = os.path.split(cwd)  # 处理一下，只提取目录名部分
cwd = cwd.replace('\\', '/')
if cwd[-1] != '/':
    cwd += '/'
os.chdir(cwd)						 # 切换工作目录 到 脚本所在目录

'''           【常见编码转换】
--- --- --- ---  URL编码转换 --- --- --- --- 
string = requests.utils.unquote('baidu%E7%99%BE%E5%BA%A6')
string = requests.utils.quote('baidu百度')

--- --- --- --- Unicode编码转换 --- --- --- 
string = r'alt="\u6ee1\u6c49\u5168\u5e2d\"/>'
string = string.encode().decode('unicode_escape')
'alt="满汉全席"\\/>'

--- --- --- --- html转义字符 &amp; --- --- ---
string = html_unescapeEntities('~^&omega;^~')
'~^ω^~'

--- --- --- --- --- 字节集 --- --- --- --- --- 
b'\xd5\xfd'.decode('gb2312')
'正'

--- --- --- --- 检测编码可能格式 --- --- --- ---
chardet.detect(r'\xd5\xfd')
{'confidence': 0.99, 'encoding': 'GB2312'}
'''



urls = '''

http://www.baidu.com

'''
urls = urls.split('\n')                                             # urls文本转换成列表
arr  = list(map(lambda t: t.strip(), urls))                         # 列表去首尾空(每一项)
urls = list(filter(lambda t: t!='', arr))                           # 列表去空项


def main():
    # 初始化session
    s = requests.Session()
    # 设置重试次数（重试3次，加上最初的一次请求，一共是4次）
    s.mount('http://', requests.adapters.HTTPAdapter(max_retries=3))
    s.mount('https://',requests.adapters.HTTPAdapter(max_retries=3))
    s.verify = False                                    # 移除服务器验证

    # 代理ip
    proxyip = ''  # '127.0.0.1:1080'
    s.proxies = {} if proxyip == '' else {
        'http': 'http://'+proxyip, 'https': 'https://'+proxyip}
        # 'http': 'socks5://'+proxyip, 'https': 'socks5://'+proxyip} # 如果是socks5的代理，用这个

    # 载入cookies
    cookiestr = ''  # 'uid=d3a794; Hm=1520578401; lp=1520578401'
    if cookiestr == '':
        cookiesdict = {}
    else:
        cookiearr   = list(map(lambda y: y.strip(), cookiestr.split(';')))
        cookiesdict = dict(map(lambda x: x.split('=',1), cookiearr))
        requests.utils.add_dict_to_cookiejar(s.cookies, cookiesdict)
    
    s.data = {}

    # 访问每个url得到返回数据
    for url in urls:
        s.headers = ReqHeader()  # {}                           # 载入一个随机网页头
        requests.utils.add_dict_to_cookiejar(s.cookies, {})     # 清空cookies
        r = s.get(url) #s.get(url,timeout=30)                   # 设置超时时间
        #r= s.post(url,allow_redirects=False)                   # 禁止重定向
        status = r.status_code                                  # 返回响应状态
        charset = HtmlCharset(r.content)                        # 返回网页编码格式               
        h = r.content.decode(encoding=charset, errors='ignore') # 返回文本（编码转换后）
        #h = r.text                                             # 返回文本
        #h = r.content                                          # 返回字节集
        #j = r.json()                                           # 返回json格式
        #c = requests.utils.dict_from_cookiejar(r.cookies)      # 返回cookies
        print(status, charset, h)
        break

        r'''
        h = middlepart(h,'color','</div>')          # 取文本或字节集中间
        h = HtmlTagRemove(h)                        # 去除html标签
        h = re.sub(r'\n\s*\n', "\n", h.replace('\r\n','\n')) # 删除空行
        arr = re.findall(r">([\s\S]*?)<", h)        # 使用正则
        soup = HtmlBS(h)                            # 创建BeautifulSoup对象

        doc = pq(url)                                  # 获取网页
        title = doc('h1').text()                       # 取标题
        content = doc('[id=content]').text()           # 取内容  # 其他写法参照 “CSS 选择器参考手册”  http://www.w3school.com.cn/cssref/css_selectors.asp
        url = doc('a:contains("下一章")').attr('href')  # 链接文字为"下一章"
        for tr in doc('tr').items():print(tr)          # 遍历每一个tr
        doc.find('div[class="hide"]').remove()         # 删除部分内容
        htmltxt = doc.html(method='html')              # 避免doc转文本时自闭合代码(比如<a></a>变成<a/>)
        
        arr = HtmlTable2List(table,indexes=[])      # table到list(可提取指定列)
        j = HtmlJson(h)                             # 展开嵌套json到单层

        HtmlDownMuti(urls,folder='')                # 批量下载
        download_from_url(url)                      # 下载带进度显示
        
        with codecs.open('temp.html','w','utf-8') as f:f.write(h)   # 按指定编码读写文件
        os.startfile('temp.html')                                   # 用系统默认应用打开文件
        '''


def HtmlRe(source):
    r''' 创建正则对象

    r'[\s\S]*?'     # 任意字符（ 匹配任意字符， .*? 匹配不了换行符）

    r'(?:\S+)'      # 忽略子项（ ?: 表示该括号不是要提取项，而是用于语法实现）
    '''
    pass


def HtmlBS(source):
    ''' 创建BeautifulSoup对象 
    
    常用例子：

    trs = soup.find('table').find_all('tr')  # 输出table标签里面的所有tr标签内容

    tag = soup.find('input',class_='hidden') # 查找class字段为hidden的input标签

    val = str(tag['value'])                  # 输出该标签value字段的值

    txt = str(tag)                           # 输出整个标签源代码

    txt = ''.join([str(i) for i in tag.contents])   # 取标签内容的文本值（文本原文）

    txt = tag.text                                  # 取标签内容的文本值（自动过滤文本值中的标签，如<br />）

    '''
    soup = BeautifulSoup(source, "html.parser")   # 创建BeautifulSoup对象
    soup.get_text(strip=True)
    return soup


def HtmlTagRemove(htmlstr):
    ''' 去除html标签'''
    tempstr = html_unescapeEntities(htmlstr)                  # 处理转义字符
    tempstr = re.compile(r'//<!\[CDATA\[[^>]*//\]\]>', re.I).sub('', tempstr)                  # 去掉CDATA
    tempstr = re.compile(r'<\s*script[^>]*>[^<]*<\s*/\s*script\s*>', re.I).sub('', tempstr)    # 去掉script
    tempstr = re.compile(r'<\s*style[^>]*>[^<]*<\s*/\s*style\s*>', re.I).sub('', tempstr)      # 去掉style
    tempstr = re.compile(r'<br\s*?/?>', re.I).sub('\n', tempstr)    # <br/>替换成\n
    tempstr = re.compile(r'</?\w+[^>]*>').sub('', tempstr)          # 去掉HTML标签
    tempstr = re.compile('<!--[^>]*-->').sub('', tempstr)           # 去掉HTML注释
    tempstr = re.compile(r'\n\s*\n').sub('\n', tempstr)             # 去掉多余的空行
    return tempstr


def HtmlTable2List(table,indexes=[]):  
    """ 将 Table html标签，转换成嵌套list
     [[,,],[,,],[,,]]

    Args:
        table: html source in table.
        indexes: choose column want to output. [] means choose all.

    Returns:
        tablelist

        [
            [cake, 800, 5.8],
            [juice, 200, 8.8]
            [pie, 50, 40]
        ]
    """
    tablelist = []
    tr_arr = re.findall(r'<tr[^>]*>([\s\S]*?)</tr>',table)
    for tr in tr_arr:
        td_arr = re.findall(r'<(?:td|th)[^>]*>([\s\S]*?)</(?:td|th)>',tr)
        if indexes==[]:
            tablelist.append(td_arr)
        else:
            temp_arr = [td_arr[i] for i in indexes]
            tablelist.append(temp_arr)
    return tablelist


def HtmlCharset(r_bytes):
    ''' 从bytes格式源码中找到字符编码标记charset

    Args:
        r_bytes: 网页源代码（字节集形式）
    
    Returns:
        charset: 返回编码格式（如果没有找到，则为空）
    '''
    head = r_bytes[r_bytes.find(b'<head>'):r_bytes.find(b'</head>')]
    arr = requests.utils.get_encodings_from_content(str(head))
    charset = arr[0] if arr else 'utf-8'
    return charset


def HtmlJson(var):
    ''' 多层json展开变成一层

        requests请求返回的json往往是复杂嵌套关系，我们将所有嵌套层展开变成单层
        例如：
        {'a':1,
        'b':{'x':11,
            'y':12},
        'c':2}
        变成
        {'a':1,'b_x':11,'b_y':12,'c':2}

    Args:
        var: json格式变量或字符串
    '''
    if isinstance(var, str):		# 检查变量类型
        j = json.loads(var)			# 字符串 到 json
    else:
        j = var

    while 1:
        nomix = True
        for k in list(j.keys()):
            if isinstance(j[k], dict):
                for kk in list(j[k].keys()):
                    j[k+'_'+kk] = j[k][kk]
                del j[k]
                nomix = False
        if nomix:
            break
    return j


def HtmlDown(url,folder='',filename='',proxyip=''):
    ''' HtmlDownMuti() 的子进程

    Args:
        url: 文件下载地址
        folder: 文件保存目录
        filenames: 文件名
        proxyip： 代理ip 例如：'127.0.0.1:1080'
    '''
    # 初始化proxies
    proxies = {} if proxyip == '' else {
        'http': 'http://'+proxyip, 'https': 'https://'+proxyip}
    # 合成文件保存地址 
    if folder  =='':folder  =os.getcwd()                # 取运行目录
    if not os.path.exists(folder):os.makedirs(folder)   # 目录不存在则创建目录
    if filename=='':                                    # 从url中提取文件名
        filename = os.path.split(url.split('#', 1)[0].split('?', 1)[0].strip())[1]
    fullpath = os.path.join(folder,filename)
    # 下载文件
    if os.path.exists(fullpath):
        # 存在同名文件，跳过下载
        print('ERROR: filename already exists! ; url = %s' % url)
    else:
        # 开始下载(最多4次重试，超时时间为30s)
        ss = requests.Session()
        ss.mount('http://', requests.adapters.HTTPAdapter(max_retries=3))
        ss.mount('https://', requests.adapters.HTTPAdapter(max_retries=3))
        try:
            r = ss.get(url, proxies=proxies, timeout=30)
        except requests.exceptions.RequestException as e:
            print(time.strftime('%Y-%m-%d %H:%M:%S'),e)                       
        with open(fullpath, "wb") as f:
            f.write(r.content)
    time.sleep(0.01) # 避免多线程时下载过于频繁


def HtmlDownMuti(urls,folder='',filenames=[],proxyip='',pip=4): # 多进程批量下载文件
    '''
    multiprocessing.Pool 多进程下载图片或网页等小文件
    Args:
        urls: 下载地址列表
        folder: 文件保存目录
        filenames: 文件名列表
        proxyip： 代理ip 例如：'127.0.0.1:1080'
        pip: 并发进程数量（和cpu核心数有关，4核cpu最大为4）
    '''

    # 检查下载地址与文件名数目是否一致
    num1 = len(urls)
    num2 = len(filenames)
    if num2!=0 and num1!=num2:
        raise Exception('下载地址数目%d 与 文件名数目不匹配%d' % (num1,num2))

    # 开始多线程下载
    print("mutiDownload start")
    pool = multiprocessing.Pool(processes=pip)     # processes=4是最多并发进程数量。
    for i in range(num1):
        url = urls[i]
        filename = '' if filenames==[] else filenames[i]
        pool.apply_async(HtmlDown, args=(url,folder,filename,proxyip)) # 注意要用apply_async，如果落下async，就变成阻塞版本了
    pool.close()
    pool.join()
    print("mutiDownload finished.")


def timestamp(long=13):
    t = time.time()
    ts = ''
    if long==10:
        ts = str(int(t))
    else:
        ts = str(int(round(t*1000)))
    return ts


def download_from_url(url,name='', path='', proxyip=''):
    """ 下载并显示进度条.

    Args:
        url: url to download file
        name: file name to save.
        path: folder to save file.
        proxyip: ip string like '127.0.0.1:1080'

    Returns:
        file_size
    """
    # 初始化头文件及代理ip
    header = ReqHeader()
    proxies = {} if proxyip == '' else {'http':'http://'+proxyip,'https':'https://'+proxyip}

    # 为request加入代理
    proxy_support = urllib.request.ProxyHandler(proxies)        #创建ProxyHandler
    opener = urllib.request.build_opener(proxy_support)         #创建Opener
    opener.addheaders = [('User-Agent',header['User-Agent'])]   #添加User Angent
    urllib.request.install_opener(opener)                       #安装OPener

    # 获取文件长度
    r = urllib.request.urlopen(url)
    file_size = int(r.info().get('Content-Length', -1))

    if name == '':
        # 从url中提取文件名
        if 'Content-Disposition' in r.info():
            fileName = r.info()['Content-Disposition'].split('filename=')[1]
            fileName = fileName.replace('"', '').replace("'", "")
        else:
            # 之所以用r.url而不是url是因为下载地址有时候会重定向
            fileName = os.path.basename(requests.utils.unquote(r.url).split('#',1)[0].split('?',1)[0].strip())
    else:
        fileName = name

    # 如果只有文件名，补上当前目录路径标识"./"
    if path == '':
        fullpath = './' + fileName
    else:
        fullpath = os.path.join(path,fileName)

    # 断点续传
    if os.path.exists(fullpath):
        first_byte = os.path.getsize(fullpath)
    else:
        first_byte = 0
    if first_byte >= file_size:
        return file_size
    header["Range"] = "bytes=%s-%s" % (first_byte, file_size)

    # 进度条
    pbar = tqdm(
        total=file_size, initial=first_byte,
        unit='B', unit_scale=True, desc=url.split('/')[-1])

    # 下载文件
    req = requests.get(url, headers=header, proxies=proxies, stream=True)
    with(open(fullpath, 'ab')) as f:
        for chunk in req.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                pbar.update(1024)
    pbar.close()
    return file_size



def middlepart(source,left,right):
    ''' 依据前后内容，取文本或字节集中间部分（不包含前后内容）

    Args:
        source: 文本或字节集
        left: 左边部分
        right: 右边部分

    Returns:
        m: 中间部分
    '''

    # 初始化 空字符''或空字节b''
    b = source[0:0]
    # 左边位置
    if left == b:               
        l = 0
    else:
        l= source.find(left)
        # 修正左边位置，跳过左边字符长度
        if l !=-1:l += len(left)
    # 右边位置
    r = source.find(right,l)
    # 取中间字符串
    if l==-1 or r==-1:
        m = b
        print('取中间内容失败:l=%d,r=%d' % (left,right))
    else:
        m = source[l:r]    
    return m



def html_unescapeEntities(html):
    ''' 处理转义字符

    处理html中的字符实体(Entities)
    将 '^&amp;omega;^' 转换成 '^ω^'
    '''
    html_parser = HTMLParser()
    oldlen = len(html)
    while 1:
        html = html_parser.unescape(html)
        newlen = len(html)
        if oldlen == newlen:
            break
        else:
            oldlen = newlen
    return html


def ReqHeader():
    ''' 返回一个随机的网页头

    生成随机网页头，避免爬虫被网站ban掉

    Returns:
        headers: 网页头

        {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, compress',
        'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6,ru;q=0.4',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': "1",
        'User-Agent': 'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52'
        }

    '''
    # 头部代理S
    UserAgents = [
        "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-US) AppleWebKit/530.9 (KHTML, like Gecko) Chrome/ Safari/530.9 ",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20",
        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)",
        "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.5; AOLBuild 4337.35; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
        "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)",
        "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322)",
        "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)",
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)",
        "Opera/9.27 (Windows NT 5.2; U; zh-cn)",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
        "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.3 (Change: 287 c9dfb30)",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2pre) Gecko/20070215 K-Ninja/2.1.1",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9) Gecko/20080705 Firefox/3.0 Kapiko/3.0",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1) Gecko/20070309 Firefox/2.0.0.3",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1) Gecko/20070803 Firefox/1.5.0.12",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2) Gecko/2008070208 Firefox/3.0.1",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Version/3.1 Safari/525.13",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 ",
        ]
    '''
    # unix系统或手机端
    UserAgents = [
        "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Fedora/1.9.0.8-1.fc10 Kazehakase/0.5.6",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.10 Chromium/27.0.1453.93 Chrome/27.0.1453.93 Safari/537.36",
        "Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5",
        "Mozilla/5.0 (Linux; U; Android 3.2; ja-jp; F-01D Build/F0001) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13 ",
        "Mozilla/5.0 (iPhone; U; CPU like Mac OS X) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/4A93 ",
        "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; da-dk) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5 ",
        "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_1 like Mac OS X; ja-jp) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8B117 Safari/6531.22.7",
        ]
    '''
    headers={
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, compress',
        'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6,ru;q=0.4',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': "1",
        'User-Agent': random.choice(UserAgents)
        }
    return headers


if __name__ == '__main__':
    main()
