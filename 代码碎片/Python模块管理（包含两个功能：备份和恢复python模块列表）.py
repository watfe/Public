#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import call
import os,sys,time


'''
Created on 2018-3-28
@author: watfe
@title: Python模块管理（包含两个功能：备份和恢复python模块列表）

1、备份模块名称版本到 requirements.txt 文件；
2、通过 requirements.txt 文件，批量安装模块。

'''




'''将当前工作目录 从 解释器所在目录 切换到 脚本所在目录(sublime不需要这个,vsc、pycharm需要)'''
# print(os.getcwd())  				 # 执行脚本解释器所在目录|当前工作目录
cwd = sys.path[0]  					 # 被初始执行脚本所在目录
if os.path.isfile(cwd):  			 # py2exe打包后，路径变成 cwd+\library.zip
	cwd = os.path.split(cwd)[0]  	 # 处理一下，只提取目录名部分
cwd = cwd.replace('\\','/')
if cwd[-1]!='/':
	cwd+='/'
os.chdir(cwd)						 # 切换工作目录 到 脚本所在目录
path = cwd+'requirements.txt'



inf = input(
	'模块管理脚本：请选择你要执行的操作:\n    1.保存模块列表到requirements.txt\n    2.通过requirements.txt安装模块\n> ')
if inf=='1':
	os.popen('pip freeze > '+path)		# 备份模块列表
	time.sleep(1)
	if os.path.exists(path):
		print('requirements.txt 文件已保存到脚本所在目录')
	else:
		print('执行保存，但未找到 requirements.txt 文件，请手动核对')
elif inf=='2':
	os.popen('pip install - r '+path)  	# 批量安装模块
else:
	print('未找到相关操作')


os.system("pause")  # 请按任意键继续. . .
