
def middlepart(source, left, right, tl=False):
    ''' 文本/字节集取中间[left:right]不包含字符本身,可为空,tl=False默认非贪婪最短匹配

    Args:
        source: 文本或字节集
        left: 左边部分(为''默认取最左)
        right: 右边部分(为''默认取最右)
        tl: 是否贪婪匹配(True:贪婪,right匹配最后一次出现位置; False:非贪婪,right匹配left后第一次出现位置)

    Returns:
        m: 中间部分
    '''

    # 初始化 left,right 为''或b''
    if type(source) != str or type(source) != bytes:
        print('middlepart()输入数据类型错误，非str/bytes: ', type(source))
        return source[0:0]  # 返回''或b''

    # 左边位置left_index
    if len(left) == 0:
        left_index = 0
    else:
        left_index = source.find(left)
        # 修正左边位置，跳过左边字符长度
        if left_index != -1:
            left_index += len(left)

    # 右边位置right_index
    if len(right) == 0:
        right_index = len(source)
    elif tl:
        right_index = source.rfind(right)
    else:
        right_index = source.find(right, left_index)

    # 取中间字符串
    if left_index == -1 or right_index == -1:
        m = source[0:0]
        # print('取中间内容失败: left_index=%d, right_index=%d' % (left,right))
    else:
        m = source[left_index:right_index]

    return m


def str_countline(string):															# 取文本行数
	# 通过'\n'出现次数+1得到行数
	num = 0 if len(string)==0 else string.count('\n')+1
	return num



'aaa'.count('a')  # 文本出现次数

# 正则替换
import re
text = 'Ssx123456...'

# 大小写敏感
re.findall('s+',text)
#['s']

# 大小写不敏感（re.I）
re.findall('s+',text,flags=re.I)
#['Ss']

# 删除或替换
re.sub(r'\d', '_', text)
#'Ssx______...'

# 匹配并交换位置（通过 \1 \2 \3）
re.sub(r'([Ss]+)(\S*?)(\d+)', r'\3是\2\1', text)
#'123456是xSs...'
