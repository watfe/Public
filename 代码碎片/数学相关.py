import itertools

# 数学相关
print(17/3)									# 除法			# 5.666666666666667
print(17//3)								# 取整			# 5
print(17%3)									# 求余			# 2
round(12.12531, 2)							# 保留2位小数	# 12.13（四舍五入）
float('%.2f' % 12.12531)					# 保留2位小数	# 12.13（四舍五入）
seed = ['a','b','c','d','e']
len(list(itertools.permutations(aaa,2))		# 排列			# A_5^2 = (5)!/(5-2)! = 5*4*3*2*1/3*2*1 = 20
len(list(itertools.combinations(aaa,2))		# 组合			# C_5^2 = A_5^2/(2)!  = 20/2*1 = 10


print(bin(10))         #十转二
print(oct(10))         #十转八
print(hex(10))         #十转16
print(int('10',8))     #八转十
print(int('10',2))     #二转十
print(int('10',16))    #16转十