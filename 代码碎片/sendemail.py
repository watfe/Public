import smtplib
from email import encoders
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase

def _format_addr(s):
	# 因为如果包含中文，需要通过Header对象进行编码
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))

def sendemail(from_addr,from_addr_pw,from_smtp_server,to_addr,message_name,message_body,attachment_path=''):
	# 邮件正文是MIMEText:
	msg = MIMEMultipart()
	msg.attach(MIMEText(message_body, 'plain', 'utf-8'))
	# 添加附件就是加上一个MIMEBase，从本地读取一个附件:
	if attachment_path!='':
		with open(attachment_path, 'rb') as f:
			attachment_name = attachment_path.split('\\')[-1].split('/')[-1]	# 附件文件名
			file_extension = attachment_name.split('.')[-1]						# 附件文件名后缀
			# 设置附件的MIME和文件名，这里是txt类型:
			mime = MIMEBase('text', file_extension, filename=attachment_name)
			# 加上必要的头信息:
			mime.add_header('Content-Disposition', 'attachment', filename=attachment_name)
			mime.add_header('Content-ID', '<0>')
			mime.add_header('X-Attachment-Id', '0')
			# 把附件的内容读进来:
			mime.set_payload(f.read())
			# 用Base64编码:
			encoders.encode_base64(mime)
			# 添加到MIMEMultipart:
			msg.attach(mime)
	else:
		pass
	# 构造邮件头
	msg['From'] = _format_addr('%s <%s>' % (from_addr,from_addr))
	msg['To'] = _format_addr('%s' % to_addr)
	msg['Subject'] = Header(message_name, 'utf-8').encode()
	# 发送邮件
	server = smtplib.SMTP(from_smtp_server, 25)
	server.set_debuglevel(1)
	server.login(from_addr, from_addr_pw)
	server.sendmail(from_addr, [to_addr], msg.as_string())
	server.quit()

def test():
	message_body = 'test'
	sendemail(from_addr='ju**********cs@126.com', from_addr_pw='itBl14pSMd82', from_smtp_server='smtp.126.com', to_addr='test@126.com', message_name='AutoSignlog', message_body=message_body, attachment_path='')

if __name__ == '__main__':
	pass
	#test()