

# 长度补齐
print('%05d' % 7)		# '00007'	# 长度为5 补零
print('%5s' % 'a')		# '    a'	# 长度为5 补空格

# 检查变量类型
isinstance(1,int)		# True

os.system("pause")      # 暂停执行
