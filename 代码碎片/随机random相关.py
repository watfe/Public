import random

# 随机数操作
random.random()				# 0.85415370477785668	# 随机一个[0,1)之间的浮点数
random.uniform(0, 100)		# 18.7356606526			# 随机一个[0,100]之间的浮点数
random.randrange(0, 100, 2)	# 44					# 随机一个[0,100)之间的偶数
random.randint(0, 100)		# 22					# 随机一个[0,100]之间的整数

# 随机字符操作
seed = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+=-" # 随机字符种子库
random.choice(seed)				# 'd'				# 随机一个字符
random.sample(seed, 3)			# ['a', 'd', 'b']	# 随机多个字符（字符可重复）
''.join(random.choice('0123456789abcdef') 
    for i in range(10))         # '10e357ad93'      # 随机指定长度字符串（字符可重复）

# 随机列表操作
random.shuffle(list)								# 列表中的元素打乱
