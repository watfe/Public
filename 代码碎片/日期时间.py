import time

# 日期时间格式化
currenttime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))	# '2017-08-07 13:45:21'

# 生成时间戳(13/10位)   '1502085979917'
def CreatTimestamp(length=13): 
	timestamp = str(int(float(time.time())*1000)) if length==13 else str(int(time.time()))
	return timestamp

# 将时间转换成时间戳
dt = "2016-05-05 20:28:54"
timeArray = time.strptime(dt, "%Y-%m-%d %H:%M:%S") #转换成时间数组
timestamp = time.mktime(timeArray)#转换成时间戳

# 重新格式化时间
dt = "2016-05-05 20:28:54"
timeArray = time.strptime(dt, "%Y-%m-%d %H:%M:%S")#转换成时间数组
dt_new = time.strftime("%Y%m%d-%H:%M:%S",timeArray) #转换成新的时间格式(20160505-20:28:54)


# 取上个星期一00:00的时间戳
def getLastMonday0000():
    num = 7
    a = time.localtime(time.time()-num*24*60*60) # 当前时间减7天
    w = a.tm_wday # 取星期几，0是星期一，1星期二，以此类推
    while w!=0:
        num+=1
        t = time.time()-num*24*60*60
        a=time.localtime(t)
        w = a.tm_wday
    dt = "%d-%02d-%02d 00:00:00" % (a.tm_year,a.tm_mon,a.tm_mday)# a为上个星期一的某个时间点，现在把时间点设置成00:00:00
    timeArray = time.strptime(dt, "%Y-%m-%d %H:%M:%S")#转换成时间数组
    timestamp = time.mktime(timeArray) #转换成时间戳
    return timestamp

# 返回带毫秒的日期时间
def get_time_stamp():
    ct = time.time()
    data_head = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(ct))
    data_secs = int(float(ct)*1000) % 1000
    time_stamp = "%s.%03d" % (data_head, data_secs)
    return time_stamp   # 2018-04-21 10:34:41.217


print(time.time())                                          # 时间戳 1498539133.655  
print(time.localtime())                                     # 时间元祖 tm_year=2017, tm_mon=6, tm_mday=27, tm_hour=12, tm_min=53, tm_sec=16, tm_wday=1, tm_yday=178, tm_isdst=0  
print(time.asctime())                                       # 时间的一种可读文本形式 'Tue Jun 27 12:53:50 2017'  
print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime()))  # 按指定文本格式输出时间 '2017-06-27 13:00:57'  

st = time.localtime(time.time())                            # 时间戳 转化成 时间元祖  
st = time.strptime('2018/1/23','%Y/%m/%d')                  # 时间文本 转化成 时间元祖  
date = time.strftime('%Y-%m-%d',st)                         # 时间元祖 转化成 时间文本  '%Y-%m-%d %H:%M:%S'  
print(date)                                                 # 前面两条函数配合着用，相当于将时间文本重新格式化。  
  
# 另外我们可以通过datetime模块来计算时间差，例如：  
import datetime  
dt1 = datetime.datetime.fromtimestamp(1517302458)  
print(dt1,type(dt1))  
dt2 = datetime.datetime.now()  
print(dt2)  
print('相差%d天零%.1f个小时'%((dt2-dt1).days,(dt2-dt1).seconds/60/60))  
''''' 
2018-01-30 16:54:18 <class 'datetime.datetime'> 
2018-02-01 16:27:47.524774 
相差1天零23.6个小时 
'''  
# 注意上面的日期虽然看起来是文本，但实际上是datetime类型的。  
# 可以通过时间戳/时间文本转换得到，然后才能进行日期时间计算。  
d1 = datetime.datetime.strptime('2017-10-16 19:21:22', '%Y-%m-%d %H:%M:%S')  
