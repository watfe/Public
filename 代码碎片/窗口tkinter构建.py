import tkinter

def creat_windows():
    win = tkinter.Tk()          #定义一个窗体  
    win.title('窗体标题')        #定义窗体标题  
    win.geometry('200x200')     #定义窗体的大小，是400X200像素  
    btn1 = tkinter.Button(win, text='按钮', command=hello)  #注意这个地方，不要写成hello(),如果是hello()的话，会在mainloop中调用hello函数，而不是单击button按钮时出发事件  
    btn1.pack()                 #将按钮pack，充满整个窗体(只有pack的组件实例才能显示)  
    win.mainloop()              #进入主循环，程序运行  

def hello():
    pass

creat_windows()