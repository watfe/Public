#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pip install selenium
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import random,time,re



'''
Created on 2018-1-04
@author: watfe
@title: selenium及phantomjs无界面浏览器的使用：取360安卓应用市场好评榜前两页数据

居然只是三个月之前的吗，整理的时候才发现，自己都不记得了。
'''


# 将当前工作目录 从 解释器所在目录 切换到 脚本所在目录(sublime不需要这个,vsc，pycharm需要)
import os
import sys
cwd = sys.path[0]  					 # 被初始执行脚本所在目录
if os.path.isfile(cwd):  			 # py2exe打包后，路径变成 cwd+\library.zip
    cwd, filen = os.path.split(cwd)  # 处理一下，只提取目录名部分
cwd = cwd.replace('\\', '/')
if cwd[-1] != '/':cwd += '/'
os.chdir(cwd)						 # 切换工作目录 到 脚本所在目录


# PTJSbrowser构造好的类，可直接调用
class PTJSbrowser:
    def __init__(self):
        # 头部代理
        UserAgents = [
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36"
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"
        ]
        # 引入配置对象DesiredCapabilities
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        service_args=[]
        useragent = random.choice(UserAgents)
        dcap["phantomjs.page.settings.userAgent"] = useragent            #从USER_AGENTS列表中随机选一个浏览器头，伪装浏览器
        dcap["phantomjs.page.settings.loadImages"] = False               #不载入图片，爬页面速度会快很多
        #service_args = ['--proxy=127.0.0.1:9999','--proxy-type=socks5'] #设置代理
        service_args.append('--load-images=false')                       #关闭图片加载
        service_args.append('--disk-cache=yes')                          #开启缓存
        service_args.append('--ignore-ssl-errors=true')                  #忽略https错误   
        # 初始化
        self.__browser = webdriver.PhantomJS(executable_path='phantomjs.exe',desired_capabilities=dcap,service_args=service_args)   # 从 http://phantomjs.org/download.html 下载压缩包，将 phantomjs.exe 移动到“C:\Users\waterd\AppData\Local\Programs\Python\Python35-32\Scripts\”中
        self.__browser.implicitly_wait(3)           # 隐式等待5秒，可以自己调节
        self.__browser.set_page_load_timeout(30)    # 以前遇到过driver.get(url)一直不返回，但也不报错的问题，这时程序会卡住，设置超时选项能解决这个问题。 类似于requests.get()的timeout选项，driver.get()没有timeout选项
        self.__browser.set_script_timeout(30)       # 设置10秒脚本超时时间

    def __del__(self):
        # 构造析构函数
        self.__browser.quit()

    def get(self,url,trytimes=3,waitsecond=3):
        """
        获取网页
        url: 网址
        trytimes: 最大重试次数
        waitsecond: 加载等待时长
        :return: selenium结构
        """
        import time
        webS = None
        for i in range(trytimes):
            try:
                self.__browser.get(url)  # 加载目录页
                time.sleep(waitsecond)   # 等待加载完毕
                self.__browser.get_screenshot_as_file('show.png') # 保存截图，异常的时候可以看看截图上的图像对不对
                webS = self.__browser
                break
            except:
                pass
        return webS

'''    ***************************************以下是代码正文***************************************    '''

def main():
    # 360应用市场好评榜前2页的soft_id共98个
    soft_ids = [2378,8840,3580,6533,3201589,120904,3277906,716427,3177041,3078230,243026,2143926,122981,3141669,2986653,37335,701491,3127877,2717419,3161479,3021592,3850067,1813722,3163071,1958782,3278686,3203095,2659947,3244621,3281547,667,2088907,3561792,3190933,2742660,3184178,3834235,3640435,2844654,3595563,3661851,3291219,3305515,1565218,3242622,3589607,3565470,3246442,2872756,2872756,2720597,3780253,2760300,1795945,3517830,2297011,9450,3262509,1975248,3251384,3095841,1654678,2398018,2504034,3793108,3011443,3579235,3246758,2516774,221862,3866894,850271,2851844,2528416,80753,44061,3025254,3010500,3572837,3586907,3760629,1718549,124667,3338327,3865236,3287966,3300274,3658416,2327563,3532826,1754364,1754843,3308473,3779856,3434567,3839259,121037,2580190]
    # 创建webdriver PhantomJS对象
    browser = PTJSbrowser()
    for soft_id in soft_ids:
        # 打开软件信息页面
        url = 'http://zhushou.360.cn/detail/index/soft_id/'+str(soft_id)
        webS = browser.get(url)
        if webS != None:
            # 提取 全部评论/好评/中评/差评 数量
            x = 'div[class="scmt-list-type"]'                   # 构造by_css_selector
            div = webS.find_element_by_css_selector(x)          # 查找元素
            text = str(div.text)                                # 取内容
            arr = re.findall(r'全部评论\((\d+)\)\s+好评\((\d+)\)\s+中评\((\d+)\)\s+差评\((\d+)\)',text)
            print(arr)
            # 提取 下载量/软件大小（至于评分不用提取了，排在前两页的都是9.9）
            x = 'div[class="pf"]'
            div = webS.find_element_by_css_selector(x)
            text = str(div.get_attribute('innerHTML'))
            arr = re.findall(r'下载：([^<]+)<[^<]+<[^>]+>([\S\.]+)',text)
            print(arr)

if __name__ == '__main__':
    main()


