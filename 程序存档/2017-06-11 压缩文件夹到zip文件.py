#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import fnmatch
import zipfile


'''
Created on 2017-6-11
@author: watfe
@title: 文件夹中所有文件，压缩到一个zip中

印象中这只是我写其他程序的一个子模块来着，用于压缩文件夹到zip包。
反正知道怎么实现就行了。

日常用不到，
直接选中所有文件夹，右键>压缩到单独文件夹 不就完事了。

'''
def files_search(path,match = ''): # 查找文件*.*
    files = [] # 初始化返回数组
    # 遍历文件夹获取所有文件到files列表
    for record in os.walk(path): 
        # walk结果形式 [(walkpath,[dirlist],[filelist]),...]
        dirname = record[0].replace('\\','/')
        for filename in record[2]:
            files.append(os.path.join(dirname+'/'+filename))
    # 匹配文件名
    if match == '':                                 # 不需要匹配，返回全部文件
        files_fnmatch = files
    else:                                           # 需要匹配
        files_fnmatch = []
        for file in files:
            basename  = os.path.basename(file)      # 取文件名
            if fnmatch.fnmatch(basename, match):    # 匹配测试
                files_fnmatch.append(file)
    return files_fnmatch


# 所有要压缩的文件夹
dirs = ['D:/100','D:/101','D:/102','D:/103'] #'D:/104/', 'D:/105/', ……  一堆文件夹

# 逐个文件夹执行压缩
for d in dirs:
    zipfiles = files_search(d) #要压缩的文件夹
    f = zipfile.ZipFile(os.path.split(d)[1] ,'w',zipfile.ZIP_DEFLATED)
    for file in zipfiles:
        f.write(file)
    f.close()

