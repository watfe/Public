#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sqlite3

'''
Created on 2018-3-13
@author: watfe
@title: 利用sqlite3模块，在通话记录备份数据库中，构造18000条通话记录，再恢复回手机里

华为mate10手机
1、备份通话记录：得到文件“calllog.db”和“info.xml”
1、通过下面的代码，伪造了18000条通话记录，到calllog.db中。
2、修改info.xml，删除checkMsgV3项及其值，recordTotal数值改为修改后数据库中的数据数。
3、拷贝回手机，通过恢复，还原到手机中
'''

# con = sqlite3.connect('calllog.db')
# for i in range(18000):
# 	names='date,new,photo_id,subscription_component_name,formatted_number,data_usage,subscription,type,numbertype,geocoded_location,ring_times,duration,presentation,subscription_id,number,features,countryiso,matched_number'
# 	d = str(1520477395000+i*5000)	# 每一条记录和前一条间隔5秒
# 	n = str(13011170001+i)			# 每一个电话号码都+1，确保号码不相同
# 	values = d+",0,0,'com.android.phone/com.android.services.telephony.TelephonyConnectionService','',0,0,2,0,'',1,3,1,'0','"+n+"',0,'CN','"+n+"'"
# 	query = "INSERT INTO "+'calls_tb'+" ("+names+") VALUES ("+values+")"
# 	con.execute(query)
# 	if i%500==0:
# 		con.commit()
# 		print(i)
# con.commit()
# con.close()



'''
Created on 2019-8-12
@author: watfe
@title: 构造18000条通话记录，再恢复回手机里

新版华为，备份的db存在验证，难以读取，且无法修改数据。
所以改用其他方法，用钛备份构造xml数据导入数据
'''

'''
<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<calls count="147" xmlns="http://www.titaniumtrack.com/ns/titanium-backup/calls">
<call ...
<call cachedNumberType="0" date="2019-03-18T04:06:59.070Z" duration="0" isNew="false" number="15011479141" type="outgoing" country="CN" geoLocation="北京 移动" />
<call ...
</calls>
'''
import datetime
import codecs
num = 18000
ts = 1517300000
html = '''<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><calls count="'''+str(num)+'''" xmlns="http://www.titaniumtrack.com/ns/titanium-backup/calls">'''
for i in range(num):
	date = datetime.datetime.fromtimestamp(ts+100*i).strftime('%Y-%m-%dT%H:%M:%S.000Z')
	call = '''<call cachedNumberType="0" date="%s" duration="0" isNew="false" number="%s" type="outgoing" country="CN" geoLocation="北京 移动" />'''%(date,str(13000000000+i))
	html+= call
else:
	html+='</calls>'
with codecs.open('xml.xml', 'w','utf-8') as f:
	f.write(html)
